/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.thidarat.oxprogram;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class testOX {

    public testOX() {
    }

    @Test
    public void TestcheckVeticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
                               {'O', '-', '-'},
                               {'O', '-', '-'}};
        char player = 'O';
        int col = 1;

        assertEquals(true, OXProgram.checkVetical(table, player, col));
    }
    
    @Test
    public void TestcheckVeticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'},
                               {'-', 'O', '-'},
                               {'-', 'O', '-'}};
        char player = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVetical(table, player, col));
    }
    
     @Test
    public void testCheckVerticleOCol3NoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char player = 'O';
        int col = 3;

        assertEquals(false, OXProgram.checkVetical(table, player, col));
    }
    
    @Test
    public void testCheckHorizontalXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;

        assertEquals(true, OXProgram.checkHrizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                {'-', '-', '-'}, 
                                {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;

        assertEquals(true, OXProgram.checkHrizontal(table, currentPlayer, row));
    }
    
    
    @Test
    public void testCheckHorizontalXRow2NoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;

        assertEquals(false, OXProgram.checkHrizontal(table, currentPlayer, row));
    }
@Test
    public void testCheckX1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                {'-', 'O', '-'}, 
                                {'-', '-', 'O'}};
        char currentPlayer = 'O';

        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2NoWin() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';

        assertEquals(false, OXProgram.checkX2(table, currentPlayer));
    }
    @Test
    public void testCheckDraw() {
        char table[][] = {{'O', 'X', 'X'}, 
                                 {'X', 'O', 'X'}, 
                                 {'X', 'X', 'O'}};
        
        int count = 8;
        
        assertEquals(true, OXProgram.checkDraw(count));
    }
  
}
