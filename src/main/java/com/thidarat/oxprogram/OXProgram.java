/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.oxprogram;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static int count = 0;
    static boolean finish = false;

    public static void main(String[] args) {
        showWelcome();

        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();

            if (finish) {
                break;
            }

        }

    }

    private static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + player);
    }

    public static void inputRowCol() {

        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    public static void process() {

        if (setTable()) {

            if (checkWin(table,player,row,col)) {
                finish = true;
                showTable();
                System.out.println(">>>" + player + " Win <<<");
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }

    }

    public static void switchPlayer() {

        if (player == 'O') {

            player = 'X';

        } else {
            player = 'O';
        }
    }

    public static boolean setTable() {

        table[row - 1][col - 1] = player;
        return true;

    }

    public static boolean checkWin(char[][] table,char player,int row,int col) {

        if (checkVetical(table, player,col)) { //ตั้ง

            return true;

        } else if (checkHrizontal(table, player,row)) {//นอน
            return true;

        } else if (checkX(table, player)) { //ทเเยง
            return true;
        }
        return false;
    }

    public static boolean checkVetical(char[][] table,char player,int col) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != player) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHrizontal(char[][] table,char player,int row) {
        for (int i = 0; i < table.length; i++) {
            if (table[row - 1][i] != player) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX(char[][] table,char player) {
        if (checkX1(table,player)) {
            return true;
        } else if (checkX2(table,player)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table,char player) { //11 22 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != player) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table,char player) { // 13 22 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != player) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(int count) {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public static void showDraw() {

        System.out.println(">>> Draw <<<");

    }
}
